import { Component, OnInit } from '@angular/core';

import { CiudadesService } from '../services/ciudades.service';
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators'


@Component({
  selector: 'app-consultas',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.scss']
})
export class ConsultasComponent implements OnInit {
    public cd:any={};
    public cl:any={};
    public verSeleccion:any = {};
    public ciudadElegida:string='Dubai';
    public descripcion:string;
    opcionSeleccionado:string='';
    Url = "http://api.openweathermap.org/data/2.5/weather?q=";


  constructor(private _estado:CiudadesService,
              private _http:HttpClient) {
  }

  ngOnInit() {
    this.getInicio();
    this.getClima();
  }

  getInicio(){
    this._estado.getmunicipios()
    .subscribe(data=>{
      this.cd=data;
    },error=>{
      console.log(error)
    })
  }

  capturar(posicion:any) {
    this.verSeleccion = this.cd[posicion];
    this.ciudadElegida=this.verSeleccion.value;
    this.getClima();
  }


  getDatos(){
    return this._http.get(this.Url+ this.ciudadElegida +"&APPID=d6b36e8607143077f63af613c52ecdb7")
           .pipe(
             map(resp=>this.crearArreglo(resp))
           );
  }

  private crearArreglo( datosClimaObj : object){
    const  clima: any[] = [];
    clima.push(datosClimaObj);
    return clima;
  }

  getClima(){
    this.getDatos()
    .subscribe(data=>{
      this.cl=data;
      for (let variable of this.cl) {
        for (let dato of variable.weather) {
           this.descripcion=dato.description;
        }
      }

    },error=>{
      console.log(error)
    })
  }



}
