import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import {Observable} from 'rxjs';
// import {map} from 'rxjs/operators'

@Injectable()
export class CiudadesService {
  Url = "./assets/json/ciudades.json"

  constructor(private _http:HttpClient) {

   }

   getmunicipios(){
     return this._http.get(this.Url)
   }

}
